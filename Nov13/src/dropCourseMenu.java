import javax.swing.JOptionPane;

public class dropCourseMenu {
	
	
			//allows user to drop courses on the class schedule
	
	public static void options()
	{
		
		String answer;

 
	
	for(String abc: AddCourseDB.classSchedule) {
		if(Schedule.newSchedule.contains(abc)) {
			
		} else
		Schedule.newSchedule = Schedule.newSchedule+abc;
	}

	
	if(Schedule.newSchedule.equals("")) {
		JOptionPane.showMessageDialog(null, "No classes currently registered.", "Schedule", 2);
		MainMenu.selection();
	}
	
			String option = JOptionPane.showInputDialog("Which subject would you like to drop?\n Enter course CCN to drop. \n Enter 0 to return. \n\n"+Schedule.newSchedule);
			

	         switch(option.toUpperCase()){
	           	case "BIO1":
	           		if(!AddCourseDB.classSchedule.contains("BIO1	        BIO 1311 - General Biology I\n")){
	            		JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
	            	}else {
	            		Capacity.BIO1cap--;
	            		Capacity.BIO1 = Capacity.BIO1cap+"/30";
	            	AddCourseDB.classSchedule.remove("BIO1	        BIO 1311 - General Biology I\n");
	           		Schedule.newSchedule = Schedule.newSchedule.replace("BIO1	        BIO 1311 - General Biology I\n", "");
	            	}
	           		break;
	            case "BIO2":
	            	if(!AddCourseDB.classSchedule.contains("BIO2	        BIO 1312 - General Biology II\n")){
	            		JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
	            	}else {
	            		Capacity.BIO2cap--;
	            		Capacity.BIO2 = Capacity.BIO2cap+"/30";
	            		AddCourseDB.classSchedule.remove("BIO2	        BIO 1312 - General Biology II\n");
	               		Schedule.newSchedule = Schedule.newSchedule.replace("BIO2	        BIO 1312 - General Biology II\n", "");
	            	}
	              	break;
	            case "BIO3":
	            	if(!AddCourseDB.classSchedule.contains("BIO3	        BIO 2412 - Organic Biology\n")){
	            		JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
	            	}else {
	             		Capacity.BIO3cap--;
	            		Capacity.BIO3 = Capacity.BIO3cap+"/30";
	            		AddCourseDB.classSchedule.remove("BIO3	        BIO 2412 - Organic Biology\n");
	               		Schedule.newSchedule = Schedule.newSchedule.replace("BIO3	        BIO 2412 - Organic Biology\n", "");
	            	}
	           		break;
	            case "BIO4":
	            	if(!AddCourseDB.classSchedule.contains("BIO4	        BIO 2415 - Molecular Biology\n")){
	            		JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
	            	}else {
	            		Capacity.BIO4cap--;
	            		Capacity.BIO4 = Capacity.BIO4cap+"/30";
	            		AddCourseDB.classSchedule.remove("BIO4	        BIO 2415 - Molecular Biology\n");
	               		Schedule.newSchedule = Schedule.newSchedule.replace("BIO4	        BIO 2415 - Molecular Biology\n", "");
	            	}
	            		break;
	            	case "ACC1":
	              		if(!AddCourseDB.classSchedule.contains("ACC1	        ACC 1311 - Introduction to Accounting I\n")){
	              			JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
	                	}else {
	                		Capacity.ACC1cap--;
	                		Capacity.ACC1 = Capacity.ACC1cap+"/30";
	                		AddCourseDB.classSchedule.remove("ACC1	        ACC 1311 - Introduction to Accounting I\n");
	                   		Schedule.newSchedule = Schedule.newSchedule.replace("ACC1	        ACC 1311 - Introduction to Accounting I\n", "");
	                	}
	              	    break;
	               case "ACC2":
	            		if(!AddCourseDB.classSchedule.contains("ACC2	        ACC 1312 - Introduction to Acounting II\n")){
	            			JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
	                	}else {
	                		Capacity.ACC2cap--;
	                		Capacity.ACC2 = Capacity.ACC2cap+"/30";
	                		AddCourseDB.classSchedule.remove("ACC2	        ACC 1312 - Introduction to Acounting II\n");
	                   		Schedule.newSchedule = Schedule.newSchedule.replace("ACC2	        ACC 1312 - Introduction to Acounting II\n", "");
	                	}
	                 	break;
	               case "ACC3":
	            		if(!AddCourseDB.classSchedule.contains("ACC3	        ACC 2411 - Intermediate Accounting I\n")){
	            			JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
	                	}else {
	                		Capacity.ACC3cap--;
	                		Capacity.ACC3 = Capacity.ACC3cap+"/30";
	                		AddCourseDB.classSchedule.remove("ACC3	        ACC 2411 - Intermediate Accounting I\n");
	                   		Schedule.newSchedule = Schedule.newSchedule.replace("ACC3	        ACC 2411 - Intermediate Accounting I\n", "");
	                	}
	              		break;
	               case "ACC4":
	            		if(!AddCourseDB.classSchedule.contains("ACC4	        ACC 2412 - Intermediate Acounting II\n")){
	            			JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
	                	}else {
	                		Capacity.ACC4cap--;
	                		Capacity.ACC4 = Capacity.ACC4cap+"/30";
	                		AddCourseDB.classSchedule.remove("ACC4	        ACC 2412 - Intermediate Acounting II\n");
	                   		Schedule.newSchedule = Schedule.newSchedule.replace("ACC4	        ACC 2412 - Intermediate Acounting II\n", "");
	                	}
	               		break;
	               case "ACC5":
	            		if(!AddCourseDB.classSchedule.contains("ACC5	        ACC 2418 - Ethics in Accounting\n")){
	            			JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
	                	}else {
	                		Capacity.ACC5cap--;
	                		Capacity.ACC5 = Capacity.ACC5cap+"/30";
	                		AddCourseDB.classSchedule.remove("ACC5	        ACC 2418 - Ethics in Accounting\n");
	                   		Schedule.newSchedule = Schedule.newSchedule.replace("ACC5	        ACC 2418 - Ethics in Accounting\n", "");
	                	}
	            			break;
	            		case "HIS1":
                       		if(!AddCourseDB.classSchedule.contains("HIS1	        HIS 1311 - United States History I\n")){
                       			JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
                        	}else {
                        		Capacity.HIS1cap--;
                        		Capacity.HIS1 = Capacity.HIS1cap+"/30";
                        		AddCourseDB.classSchedule.remove("HIS1	        HIS 1311 - United States History I\n");
                           		Schedule.newSchedule = Schedule.newSchedule.replace("HIS1	        HIS 1311 - United States History I\n", "");
                        	}
                       	    break;
                        case "HIS2":
                        	if(!AddCourseDB.classSchedule.contains("HIS2	        HIS 1312 - United States History II\n")){
                        		JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
                        	}else {
                        		Capacity.HIS2cap--;
                        		Capacity.HIS2 = Capacity.HIS2cap+"/30";
                        		AddCourseDB.classSchedule.remove("HIS2	        HIS 1312 - United States History II\n");
                           		Schedule.newSchedule = Schedule.newSchedule.replace("HIS2	        HIS 1312 - United States History II\n", "");
                        	}
                          	break;
                        case "HIS3":
                        	if(!AddCourseDB.classSchedule.contains("HIS3	        HIS 1314 - Texas History\n")){
                        		JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
                        	}else {
                        		Capacity.HIS3cap--;
                        		Capacity.HIS3 = Capacity.HIS3cap+"/30";
                        		AddCourseDB.classSchedule.remove("HIS3	        HIS 1314 - Texas History\n");
                           		Schedule.newSchedule = Schedule.newSchedule.replace("HIS3	        HIS 1314 - Texas History\n", "");
                        	}
                       		break;
                        case "HIS4":
                        	if(!AddCourseDB.classSchedule.contains("HIS4	        HIS 2318 - History of Europe\n")){
                        		JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
                        	}else {
                        		Capacity.HIS4cap++;
                        		Capacity.HIS4 = Capacity.HIS4cap+"/30";
                        		AddCourseDB.classSchedule.remove("HIS4	        HIS 2318 - History of Europe\n");
                           		Schedule.newSchedule = Schedule.newSchedule.replace("HIS4	        HIS 2318 - History of Europe\n", "");
                        	}
                        		break;
                        	case "GOV1":
                          		if(!AddCourseDB.classSchedule.contains("GOV1	        GOV 1311 - American Government I\n")){
                          			JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
                            	}else {
                            		Capacity.GOV1cap--;
                            		Capacity.GOV1 = Capacity.GOV1cap+"/30";
                            		AddCourseDB.classSchedule.remove("GOV1	        GOV 1311 - American Government I\n");
                               		Schedule.newSchedule = Schedule.newSchedule.replace("GOV1	        GOV 1311 - American Government I\n", "");
                            	}
                          	    break;
                           case "GOV2":
                        		if(!AddCourseDB.classSchedule.contains("GOV2	        GOV 1312 - American Government II\n")){
                        			JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
                            	}else {
                            		Capacity.GOV2cap--;
                            		Capacity.GOV2 = Capacity.GOV2cap+"/30";
                            		AddCourseDB.classSchedule.remove("GOV2	        GOV 1312 - American Government II\n");
                               		Schedule.newSchedule = Schedule.newSchedule.replace("GOV2	        GOV 1312 - American Government II\n", "");
                            	}
                             	break;
                           case "GOV3":
                        		if(!AddCourseDB.classSchedule.contains("GOV3	        GOV 1314 - Texas Government\n")){
                        			JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
                            	}else {
                            		Capacity.GOV3cap--;
                            		Capacity.GOV3 = Capacity.GOV3cap+"/30";
                            		AddCourseDB.classSchedule.remove("GOV3	        GOV 1314 - Texas Government\n");
                               		Schedule.newSchedule = Schedule.newSchedule.replace("GOV3	        GOV 1314 - Texas Government\n", "");
                            	}
                          		break;
                           case "GOV4":
                        		if(!AddCourseDB.classSchedule.contains("GOV4	        GOV 2318 - U.S. ELectoral System\n")){
                        			JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
                            	}else {
                            		Capacity.GOV4cap--;
                            		Capacity.GOV4 = Capacity.GOV4cap+"/30";
                            		AddCourseDB.classSchedule.remove("GOV4	        GOV 2318 - U.S. ELectoral System\n");
                               		Schedule.newSchedule = Schedule.newSchedule.replace("GOV4	        GOV 2318 - U.S. ELectoral System\n", "");
                            	}
                        			break;
                        		case "MGT1":
                              		if(!AddCourseDB.classSchedule.contains("MGT1	        MGT 1311 - Introduction to Management\n")){
                              			JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
                                	}else {
                                		Capacity.MGT1cap--;
                                		Capacity.MGT1 = Capacity.MGT1cap+"/30";
                                		AddCourseDB.classSchedule.remove("MGT1	        MGT 1311 - Introduction to Management\n");
                                   		Schedule.newSchedule = Schedule.newSchedule.replace("MGT1	        MGT 1311 - Introduction to Management\n", "");
                                	}
                              	    break;
                               case "MGT2":
                            		if(!AddCourseDB.classSchedule.contains("MGT2	        MGT 1312 - Issues in Management\n")){
                            			JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
                                	}else {
                                		Capacity.MGT2cap--;
                                		Capacity.MGT2 = Capacity.MGT2cap+"/30";
                                		AddCourseDB.classSchedule.remove("MGT2	        MGT 1312 - Issues in Management\n");
                                   		Schedule.newSchedule = Schedule.newSchedule.replace("MGT2	        MGT 1312 - Issues in Management\n", "");
                                	}
                                 	break;
                               case "MGT3":
                            		if(!AddCourseDB.classSchedule.contains("MGT3	        MGT 2314 - Science of Management\n")){
                            			JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
                                	}else {
                                		Capacity.MGT3cap--;
                                		Capacity.MGT3 = Capacity.MGT3cap+"/30";
                                		AddCourseDB.classSchedule.remove("MGT3	        MGT 2314 - Science of Management\n");
                                   		Schedule.newSchedule = Schedule.newSchedule.replace("MGT3	        MGT 2314 - Science of Management\n", "");
                                	}
                              		break;
                               case "MGT4":
                            		if(!AddCourseDB.classSchedule.contains("MGT4	        MGT 2318 - Ethics in Management\n")){
                            			JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
                                	}else {
                                		Capacity.MGT4cap--;
                                		Capacity.MGT4 = Capacity.MGT4cap+"/30";
                                		AddCourseDB.classSchedule.remove("MGT4	        MGT 2318 - Ethics in Management\n");
                                   		Schedule.newSchedule = Schedule.newSchedule.replace("MGT4	        MGT 2318 - Ethics in Management\n", "");
                                	}
                            			break;
                            		case "MAT1":
                                 		if(!AddCourseDB.classSchedule.contains("MAT1	        MAT 1311 - Business Algebra\n")){
                                 			JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
                                    	}else {
                                    		Capacity.MAT1cap--;
                                    		Capacity.MAT1 = Capacity.MAT1cap+"/30";
                                    		AddCourseDB.classSchedule.remove("MAT1	        MAT 1311 - Business Algebra\n");
                                       		Schedule.newSchedule = Schedule.newSchedule.replace("MAT1	        MAT 1311 - Business Algebra\n", "");
                                    	}
                                 	    break;
                                  case "MAT2":
                                		if(!AddCourseDB.classSchedule.contains("MAT2	        MAT 1312 - Business Calculus\n")){
                                			JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
                                    	}else {
                                    		Capacity.MAT2cap--;
                                    		Capacity.MAT2 = Capacity.MAT2cap+"/30";
                                    		AddCourseDB.classSchedule.remove("MAT2	        MAT 1312 - Business Calculus\n");
                                       		Schedule.newSchedule = Schedule.newSchedule.replace("MAT2	        MAT 1312 - Business Calculus\n", "");
                                    	}
                                    	break;
                                  case "MAT3":
                                		if(!AddCourseDB.classSchedule.contains("MAT3	        MAT 2314 - Statistics\n")){
                                			JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
                                    	}else {
                                    		Capacity.MAT3cap--;
                                    		Capacity.MAT3 = Capacity.MAT3cap+"/30";
                                    		AddCourseDB.classSchedule.remove("MAT3	        MAT 2314 - Statistics\n");
                                       		Schedule.newSchedule = Schedule.newSchedule.replace("MAT3	        MAT 2314 - Statistics\n", "");
                                    	}
                                 		break;
                                  case "MAT4":
                                		if(!AddCourseDB.classSchedule.contains("MAT4	        MAT 2318 - Calculus\n")){
                                			JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
                                    	}else {
                                    		Capacity.MAT4cap--;
                                    		Capacity.MAT4 = Capacity.MAT4cap+"/30";
                                    		AddCourseDB.classSchedule.remove("MAT4	        MAT 2318 - Calculus\n");
                                       		Schedule.newSchedule = Schedule.newSchedule.replace("MAT4	        MAT 2318 - Calculus\n", "");
                                    	}
                                			break;
                                		case "ART1":
                                   			if(!AddCourseDB.classSchedule.contains("ART1	        ART 1311 - Painting I\n")){
                                   				JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
                                        	}else {
                                        		Capacity.ART1cap--;
                                        		Capacity.ART1 = Capacity.ART1cap+"/30";
                                        		AddCourseDB.classSchedule.remove("ART1	        ART 1311 - Painting I\n");
                                           		Schedule.newSchedule = Schedule.newSchedule.replace("ART1	        ART 1311 - Painting I\n", "");
                                        	}
                                   			break;
                                   		case "ART2":
                                   			if(!AddCourseDB.classSchedule.contains("ART2	        ART 1312 - Painting II\n")){
                                   				JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
                                        	}else {
                                        		Capacity.ART2cap--;
                                        		Capacity.ART2 = Capacity.ART2cap+"/30";
                                        		AddCourseDB.classSchedule.remove("ART2	        ART 1312 - Painting II\n");
                                           		Schedule.newSchedule = Schedule.newSchedule.replace("ART2	        ART 1312 - Painting II\n", "");
                                        	}
                                   			break;
                                   		case "ART3":
                                   			if(!AddCourseDB.classSchedule.contains("ART3	        ART 2314 - Photography\n")){
                                   				JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
                                        	}else {
                                        		Capacity.ART3cap--;
                                        		Capacity.ART3 = Capacity.ART3cap+"/30";
                                        		AddCourseDB.classSchedule.remove("ART3	        ART 2314 - Photography\n");
                                           		Schedule.newSchedule = Schedule.newSchedule.replace("ART3	        ART 2314 - Photography\n", "");
                                        	}
                                   			break;
                                   		case "ART4":
                                   			if(!AddCourseDB.classSchedule.contains("ART4	        ART 2318 - Drawing\n")){
                                   				JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
                                        	}else {
                                        		Capacity.ART4cap--;
                                        		Capacity.ART4 = Capacity.ART4cap+"/30";
                                        		AddCourseDB.classSchedule.remove("ART4	        ART 2318 - Drawing\n");
                                           		Schedule.newSchedule = Schedule.newSchedule.replace("ART4	        ART 2318 - Drawing\n", "");
                                        	}
                                   				break;
                                   			case "CSI1":
                            					if(!AddCourseDB.classSchedule.contains("CSI1	        CSI 1311 - JAVA I\n")){
                            						JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
                            	            	}else {
                            	            		Capacity.CSI1cap--;
                            	            		Capacity.CSI1 = Capacity.CSI1cap+"/30";
                            	            		AddCourseDB.classSchedule.remove("CSI1	        CSI 1311 - JAVA I\n");
                            	               		Schedule.newSchedule = Schedule.newSchedule.replace("CSI1	        CSI 1311 - JAVA I\n", "");
                            	            	}
                            					break;
                            				case "CSI2":
                            					if(!AddCourseDB.classSchedule.contains("CSI2	        CSI 1312 - JAVA II\n")){
                            						JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
                            	            	}else {
                            	            		Capacity.CSI2cap--;
                            	            		Capacity.CSI2 = Capacity.CSI2cap+"/30";
                            	            		AddCourseDB.classSchedule.remove("CSI2	        CSI 1312 - JAVA II\n");
                            	               		Schedule.newSchedule = Schedule.newSchedule.replace("CSI2	        CSI 1312 - JAVA II\n", "");
                            	            	}
                            					break;
                            				case "CSI3":
                            					if(!AddCourseDB.classSchedule.contains("CSI3	        CSI 1314 - Systems Design\n")){
                            						JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
                            	            	}else {
                            	            		Capacity.CSI3cap--;
                            	            		Capacity.CSI3 = Capacity.CSI3cap+"/30";
                            	            		AddCourseDB.classSchedule.remove("CSI3	        CSI 1314 - Systems Design\n");
                            	               		Schedule.newSchedule = Schedule.newSchedule.replace("CSI3	        CSI 1314 - Systems Design\n", "");
                            	            	}
                            					break;
                            				case "CSI4":
                            					if(!AddCourseDB.classSchedule.contains("CSI4	        CSI 2318 - C++\n")){
                            						JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
                            	            	}else {
                            	            		Capacity.CSI4cap--;
                            	            		Capacity.CSI4 = Capacity.CSI4cap+"/30";
                            	            		AddCourseDB.classSchedule.remove("CSI4	        CSI 2318 - C++\n");
                            	               		Schedule.newSchedule = Schedule.newSchedule.replace("CSI4	        CSI 2318 - C++\n", "");
                            	            	}
                            					break;
                            					case "CHM1":
                            						if(!AddCourseDB.classSchedule.contains("CHM1	        CHM 1311 - General Chemistry I\n")){
                            							JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
                            			        	}else {
                            			        		Capacity.CHM1cap--;
                            			        		Capacity.CHM1 = Capacity.CHM1cap+"/30";
                            			        		AddCourseDB.classSchedule.remove("CHM1	        CHM 1311 - General Chemistry I\n");
                            			           		Schedule.newSchedule = Schedule.newSchedule.replace("CHM1	        CHM 1311 - General Chemistry I\n", "");
                            			        	}
                            						break;
                            					case "CHM2":
                            						if(!AddCourseDB.classSchedule.contains("CHM2	        CHM 1312 - General Chemistry II\n")){
                            							JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
                            			        	}else {
                            			        		Capacity.CHM2cap--;
                            			        		Capacity.CHM2 = Capacity.CHM2cap+"/30";
                            			        		AddCourseDB.classSchedule.remove("CHM2	        CHM 1312 - General Chemistry II\n");
                            			           		Schedule.newSchedule = Schedule.newSchedule.replace("CHM2	        CHM 1312 - General Chemistry II\n", "");
                            			        	}
                            						break;
                            					case "CHM3":
                            						if(!AddCourseDB.classSchedule.contains("CHM3	        CHM 2314 - Intermediate Chemistry\n")){
                            							JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
                            			        	}else {
                            			        		Capacity.CHM3cap--;
                            			        		Capacity.CHM3 = Capacity.CHM3cap+"/30";
                            			        		AddCourseDB.classSchedule.remove("CHM3	        CHM 2314 - Intermediate Chemistry\n");
                            			           		Schedule.newSchedule = Schedule.newSchedule.replace("CHM3	        CHM 2314 - Intermediate Chemistry\n", "");
                            			        	}
                            						break;
                            					case "CHM4":
                            						if(!AddCourseDB.classSchedule.contains("CHM4	        CHM 2318 - Volatile Chemistry\n")){
                            							JOptionPane.showMessageDialog(null, "This class is not currently registered", "Error", 0);
                            			        	}else {
                            			        		Capacity.CHM4cap--;
                            			        		Capacity.CHM4 = Capacity.CHM4cap+"/30";
                            			        		AddCourseDB.classSchedule.remove("CHM4	        CHM 2318 - Volatile Chemistry\n");
                            			           		Schedule.newSchedule = Schedule.newSchedule.replace("CHM4	        CHM 2318 - Volatile Chemistry\n", "");
                            			        	}
                            						break;
                            					case "0":
                            						MainMenu.selection();
                            						break;
	            default:
	            	dropCourseMenu.options();
	            	break;
	           	}
	       
	         answer=  JOptionPane.showInputDialog("Drop another course?\n"
					   + "    Y/N   \n");

	      if(answer.equalsIgnoreCase("y"))
	    	  dropCourseMenu.options();
				else 
				AddDropMenu.addDrop();
			
			
			
			
			
			


	    

	}
	
	

}
