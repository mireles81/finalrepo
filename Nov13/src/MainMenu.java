
import javax.swing.JOptionPane;

public class MainMenu {
	 
	

	//displays main menu

    public static void selection() {

                 String selection =  JOptionPane.showInputDialog("Welcome Jane Doe.\n"

                                                    + "Choose a selection from below:\n"

                                                    + "Enter  1 to view Current Schedule\n"

                                                    + "Enter  2 to do a Class Search\n"

                                                    + "Enter  3 to Add/Drop\n"

                                                    + "Enter  4 to log out");
     
    	switch(selection){
    	case "1":
    		Schedule.viewSchedule();
    		break;
       	case "2":
       		Classes.classes();
       		break;
       	case "3":
       		AddDropMenu.addDrop();
       		break;
       	case "4":
       		JOptionPane.showMessageDialog(null, "Thank you for visiting.");
       		System.exit(0);
       		break;
       	default:
       		MainMenu.selection();
         		break;
    	}
    

}
}