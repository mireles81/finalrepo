

import java.util.ArrayList;

import javax.swing.JOptionPane;

public class AddCourseDB {

	
	
		//stores classes added
		static ArrayList<String> classSchedule = new ArrayList<>();
		
	
	
	
	
		  //following methods check to see if course has already been added, and/or if there is space available.   also displays number of slots available for the course
		
    public static void biology() {

    	String answer;
    
    	
    	
        String biology=  JOptionPane.showInputDialog("Select Biology Course to add.\n"
					+ "  Enter the CCN to add.\n"
					+ "	CCN   	    	 Course Description	        	           	    	 Capacity\n"
					+ "	 --------------------------------------------------------------------\n"
					+ "BIO1	        BIO 1311 - General Biology I	        	   "+Capacity.BIO1+"\n"
					+ "BIO2	        BIO 1312 - General Biology II	        	  "+Capacity.BIO2+"\n"
					+ "BIO3	        BIO 2412 - Organic Biology	        	     "+Capacity.BIO3+"\n"
					+ "BIO4	        BIO 2415 - Molecular Biology	        	 "+Capacity.BIO4+"\n"
					+ "\n"
					+ "Enter 0 to return to previous screen."
					);
         //add biology courses to array
            
        
         switch(biology.toUpperCase()){
           	case "BIO1":
           		if(AddCourseDB.classSchedule.contains("BIO1	        BIO 1311 - General Biology I\n")){
            		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
            	}else if(Capacity.BIO1cap>29){
            		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
            	} else {
            		Capacity.BIO1cap++;
            		Capacity.BIO1 = Capacity.BIO1cap+"/30";
           		AddCourseDB.classSchedule.add("BIO1	        BIO 1311 - General Biology I\n"); 
            	}
           		break;
            case "BIO2":
            	if(AddCourseDB.classSchedule.contains("BIO2	        BIO 1312 - General Biology II\n")){
            		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
            	}else if(Capacity.BIO2cap>29){
            		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
            	}else {
            		Capacity.BIO2cap++;
            		Capacity.BIO2 = Capacity.BIO2cap+"/30";
            	AddCourseDB.classSchedule.add("BIO2	        BIO 1312 - General Biology II\n");
            	}
              	break;
            case "BIO3":
            	if(AddCourseDB.classSchedule.contains("BIO3	        BIO 2412 - Organic Biology\n")){
            		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
            	}else if(Capacity.BIO3cap>29){
            		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
            	}else {
            		Capacity.BIO3cap++;
            		Capacity.BIO3 = Capacity.BIO3cap+"/30";
            	AddCourseDB.classSchedule.add("BIO3	        BIO 2412 - Organic Biology\n");
            	}
           		break;
            case "BIO4":
            	if(AddCourseDB.classSchedule.contains("BIO4	        BIO 2415 - Molecular Biology\n")){
            		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
            	}else if(Capacity.BIO4cap>29){
            		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
            	}else {
            		Capacity.BIO4cap++;
            		Capacity.BIO4 = Capacity.BIO4cap+"/30";
            	AddCourseDB.classSchedule.add("BIO4	        BIO 2415 - Molecular Biology\n");
            	}
            	break;
            case "0":
            	addCourseMenu.options();
            	break;
            default:
              	AddCourseDB.biology();
            	break;
           	}
       
         answer=  JOptionPane.showInputDialog("Add another course?\n"
				   + "    Y/N   \n");

      if(answer.equalsIgnoreCase("y"))
    	  addCourseMenu.options();
			else 
			AddDropMenu.addDrop();
   
    }  
    	
    
	public static void accounting() {

        String accounting = JOptionPane.showInputDialog("Select Accounting course to add.\n"
        									+ "Enter the CCN to add.\n"
        									+ "	CCN   	    	 Course Description	        	           	                                 Capacity\n"
        									+ "	 -------------------------------------------------------------------------------------\n"
       		 								+ "ACC1	        ACC 1311 - Introduction to Accounting I	        	       "+Capacity.ACC1+"\n"
       		 								+ "ACC2	        ACC 1312 - Introduction to Accounting II	        	      "+Capacity.ACC2+"\n"
       		 								+ "ACC3	        ACC 2411 - Intermediate Accounting I	        	          "+Capacity.ACC3+"\n"
       		 								+ "ACC4	        ACC 2412 - Intermediate Accounting II	        	         "+Capacity.ACC4+"\n"
       		 								+ "ACC5	        ACC 2418 - Ethics in Accounting	        	                    "+Capacity.ACC5+"\n"
       		 								+ "\n"
       		 								+ "Enter 0 to return to previous screen."
       		 												);
        String answer;
		
            switch(accounting.toUpperCase()){
              	case "ACC1":
              		if(AddCourseDB.classSchedule.contains("ACC1	        ACC 1311 - Introduction to Accounting I\n")){
                		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
              		}else if(Capacity.ACC1cap>29){
                		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
                	}else {
                		Capacity.ACC1cap++;
                		Capacity.ACC1 = Capacity.ACC1cap+"/30";
              	classSchedule.add("ACC1	        ACC 1311 - Introduction to Accounting I\n");
                	}
              	    break;
               case "ACC2":
            		if(AddCourseDB.classSchedule.contains("ACC2	        ACC 1312 - Introduction to Acounting II\n")){
                		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
            		}else if(Capacity.ACC2cap>29){
                		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
                	}else {
                		Capacity.ACC2cap++;
                		Capacity.ACC2 = Capacity.ACC2cap+"/30";
               	classSchedule.add("ACC2	        ACC 1312 - Introduction to Acounting II\n");
                	}
                 	break;
               case "ACC3":
            		if(AddCourseDB.classSchedule.contains("ACC3	        ACC 2411 - Intermediate Accounting I\n")){
                		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
            		}else if(Capacity.ACC3cap>29){
                		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
                	}else {
                		Capacity.ACC3cap++;
                		Capacity.ACC3 = Capacity.ACC3cap+"/30";
               	classSchedule.add("ACC3	        ACC 2411 - Intermediate Accounting I\n");
                	}
              		break;
               case "ACC4":
            		if(AddCourseDB.classSchedule.contains("ACC4	        Acc 2412 - Intermediate Acounting II\n")){
                		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
            		}else if(Capacity.ACC4cap>29){
                		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
                	}else {
                		Capacity.ACC4cap++;
                		Capacity.ACC4 = Capacity.ACC4cap+"/30";
               	classSchedule.add("ACC4	        ACC 2412 - Intermediate Acounting II\n");
                	}
               		break;
               case "ACC5":
            		if(AddCourseDB.classSchedule.contains("ACC5	        Acc 2418 - Ethics in Accounting\n")){
                		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
            		}else if(Capacity.ACC5cap>29){
                		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
                	}else {
                		Capacity.ACC5cap++;
                		Capacity.ACC5 = Capacity.ACC5cap+"/30";
                  	classSchedule.add("ACC5	        ACC 2418 - Ethics in Accounting\n");
                	}
               	break;
               case "0":
               	addCourseMenu.options();
               	break;
               default:
                 	AddCourseDB.accounting();
               	break;
              	}
          
            answer=  JOptionPane.showInputDialog("Add another course?\n"
					   + "    Y/N   \n");

            if(answer.equalsIgnoreCase("y"))
            	addCourseMenu.options();
				else 
				AddDropMenu.addDrop();
}



    public static void history() {

                 String history = JOptionPane.showInputDialog("Select History courses to add.\n"
                		 								+ "Enter the CCN to add.\n"
                		 								+ "	CCN   	    	 Course Description	        	           	    	              Capacity\n"
                		 								+ "	 ----------------------------------------------------------------------------\n"
                		 								+ "HIS1	        HIS 1311 - United States History I               "+Capacity.HIS1+"\n"
                		 								+ "HIS2	        HIS 1312 - United States History II              "+Capacity.HIS2+"\n"
                		 								+ "HIS3	        HIS 1314 - Texas History                               "+Capacity.HIS3+"\n"
                		 								+ "HIS4	        HIS 2318 - History of Europe                        "+Capacity.HIS4+"\n"
                		 								+ "\n"
                		 								+ "Enter 0 to return to previous screen."		 								
                		 );
                                                 
                 String answer;
         		
                     switch(history.toUpperCase()){
                       	case "HIS1":
                       		if(AddCourseDB.classSchedule.contains("HIS1	        HIS 1311 - United States History I\n")){
                        		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
                       		}else if(Capacity.HIS1cap>29){
                        		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
                        	}else {
                        		Capacity.HIS1cap++;
                        		Capacity.HIS1 = Capacity.HIS1cap+"/30";
                       	classSchedule.add("HIS1	        HIS 1311 - United States History I\n");
                        	}
                       	    break;
                        case "HIS2":
                        	if(AddCourseDB.classSchedule.contains("HIS2	        HIS 1312 - United States History II\n")){
                        		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
                        	}else if(Capacity.HIS2cap>29){
                        		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
                        	}else {
                        		Capacity.HIS2cap++;
                        		Capacity.HIS2 = Capacity.HIS2cap+"/30";
                        	classSchedule.add("HIS2	        HIS 1312 - United States History II\n");
                        	}
                          	break;
                        case "HIS3":
                        	if(AddCourseDB.classSchedule.contains("HIS3	        HIS 1314 - Texas History\n")){
                        		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
                        	}else if(Capacity.HIS3cap>29){
                        		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
                        	}else {
                        		Capacity.HIS3cap++;
                        		Capacity.HIS3 = Capacity.HIS3cap+"/30";
                        	classSchedule.add("HIS3	        HIS 1314 - Texas History\n");
                        	}
                       		break;
                        case "HIS4":
                        	if(AddCourseDB.classSchedule.contains("HIS4	        HIS 2318 - History of Europe\n")){
                        		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
                        	}else if(Capacity.HIS4cap>29){
                        		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
                        	}else {
                        		Capacity.HIS4cap++;
                        		Capacity.HIS4 = Capacity.HIS4cap+"/30";
                        	classSchedule.add("HIS4	        HIS 2318 - History of Europe\n");
                        	}
                        	break;
                        case "0":
                        	addCourseMenu.options();
                        	break;
                        default:
                          	AddCourseDB.history();
                        	break;
                       	}
                   
                     	 answer=  JOptionPane.showInputDialog("Add another course?\n"
                        											   + "    Y/N   \n");
                    	 
    					
    				if(answer.equalsIgnoreCase("y"))
    					addCourseMenu.options();
    					else 
    					AddDropMenu.addDrop();
                    		
}


	

    public static void government() {

                String government = JOptionPane.showInputDialog("Select Government courses to add.\n"
														+ "Enter the CCN to add.\n"
														+ "	CCN   	    	 Course Description	        	           	    	                   Capacity\n"
														+ "	 ----------------------------------------------------------------------------------\n"
                		 								+ "GOV1	        GOV 1311 - American Government I                 "+Capacity.GOV1+"\n"
                		 								+ "GOV2	        GOV 1312 - American Government II                "+Capacity.GOV2+"\n"
                		 								+ "GOV3	        GOV 1314 - Texas Government                         "+Capacity.GOV3+"\n"
                		 								+ "GOV4	        GOV 2318 - U.S. Electoral System                    "+Capacity.GOV4+"\n"
                		 								+ "\n"
                		 								+ "Enter 0 to return to previous screen." 					                		 								
                		);
                                                 
                 
String answer;

    switch(government.toUpperCase()){
      	case "GOV1":
      		if(AddCourseDB.classSchedule.contains("GOV1	        GOV 1311 - American Government I\n")){
        		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
      		}else if(Capacity.GOV1cap>29){
        		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
        	}else {
        		Capacity.GOV1cap++;
        		Capacity.GOV1 = Capacity.GOV1cap+"/30";
      	classSchedule.add("GOV1	        GOV 1311 - American Government I\n");
        	}
      	    break;
       case "GOV2":
    		if(AddCourseDB.classSchedule.contains("GOV2	        GOV 1312 - American Government II\n")){
        		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
    		}else if(Capacity.GOV2cap>29){
        		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
        	}else {
        		Capacity.GOV2cap++;
        		Capacity.GOV2 = Capacity.GOV2cap+"/30";
       	classSchedule.add("GOV2	        GOV 1312 - American Government II\n");
        	}
         	break;
       case "GOV3":
    		if(AddCourseDB.classSchedule.contains("GOV3	        GOV 1314 - Texas Government\n")){
        		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
    		}else if(Capacity.GOV3cap>29){
        		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
        	}else {
        		Capacity.GOV3cap++;
        		Capacity.GOV3 = Capacity.GOV3cap+"/30";
       	classSchedule.add("GOV3	        GOV 1314 - Texas Government\n");
        	}
      		break;
       case "GOV4":
    		if(AddCourseDB.classSchedule.contains("GOV4	        GOV 2318 - U.S. ELectoral System\n")){
        		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
    		}else if(Capacity.GOV4cap>29){
        		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
        	}else {
        		Capacity.GOV4cap++;
        		Capacity.GOV4 = Capacity.GOV4cap+"/30";
       	classSchedule.add("GOV4	        GOV 2318 - U.S. ELectoral System\n");
        	}
       	break;
       case "0":
       	addCourseMenu.options();
       	break;
       default:
         	AddCourseDB.government();
       	break;
      	}
  
    	 answer=  JOptionPane.showInputDialog("Add another course?\n"
       											   + "    Y/N   \n");
   	 
		
	if(answer.equalsIgnoreCase("y"))
		addCourseMenu.options();
		else 
		AddDropMenu.addDrop();
}
    


    public static void management() {

                 String management = JOptionPane.showInputDialog("Select Management courses to add.\n"
                		 								+ "Enter the CCN to add.\n"
                		 								+ "	CCN   	    	 Course Description	        	           	    	                     Capacity\n"
                		 								+ "	 ----------------------------------------------------------------------------------------\n"
                		 								+ "MGT1	        MGT 1311 - Introduction to Management        "+Capacity.MGT1+"\n"
                		 								+ "MGT2	        MGT 1312 - Issues in Management                  "+Capacity.MGT2+"\n"
                		 								+ "MGT3	        MGT 2314 - Science of Management               "+Capacity.MGT3+"\n"
                		 								+ "MGT4	        MGT 2318 - Ethics in Management                  "+Capacity.MGT4+"\n"
                		 								+ "\n"
                		 								+ "Enter 0 to return to previous screen."					                		 						
                		 );
                                                 
                 
String answer;

    switch(management.toUpperCase()){
      	case "MGT1":
      		if(AddCourseDB.classSchedule.contains("MGT1	        MGT 1311 - Introduction to Management\n")){
        		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
      		}else if(Capacity.MGT1cap>29){
        		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
        	}else {
        		Capacity.MGT1cap++;
        		Capacity.MGT1 = Capacity.MGT1cap+"/30";
      	classSchedule.add("MGT1	        MGT 1311 - Introduction to Management\n");
        	}
      	    break;
       case "MGT2":
    		if(AddCourseDB.classSchedule.contains("MGT2	        MGT 1312 - Issues in Management\n")){
        		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
    		}else if(Capacity.MGT2cap>29){
        		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
        	}else {
        		Capacity.MGT2cap++;
        		Capacity.MGT2 = Capacity.MGT2cap+"/30";
       	classSchedule.add("MGT2	        MGT 1312 - Issues in Management\n");
        	}
         	break;
       case "MGT3":
    		if(AddCourseDB.classSchedule.contains("MGT3	        MGT 2314 - Science of Management\n")){
        		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
    		}else if(Capacity.MGT3cap>29){
        		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
        	}else {
        		Capacity.MGT3cap++;
        		Capacity.MGT3 = Capacity.MGT3cap+"/30";
       	classSchedule.add("MGT3	        MGT 2314 - Science of Management\n");
        	}
      		break;
       case "MGT4":
    		if(AddCourseDB.classSchedule.contains("MGT4	        MGT 2318 - Ethics in Management\n")){
        		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
    		}else if(Capacity.MGT4cap>29){
        		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
        	}else {
        		Capacity.MGT4cap++;
        		Capacity.MGT4 = Capacity.MGT4cap+"/30";
       	classSchedule.add("MGT4	        MGT 2318 - Ethics in Management\n");
        	}
       	break;
       case "0":
       	addCourseMenu.options();
       	break;
       default:
         	AddCourseDB.management();
       	break;
      	}
  
    	 answer=  JOptionPane.showInputDialog("Add another course?\n"
       											   + "    Y/N   \n");
   	 
		
	if(answer.equalsIgnoreCase("y"))
		addCourseMenu.options();
		else 
		AddDropMenu.addDrop();
}

    public static void math() {

                String math = JOptionPane.showInputDialog("Select Math courses to add.\n"
                										+ "Enter the CCN to add.\n"
                										+ "	CCN   	    	 Course Description	        	           	    	 Capacity\n"
                										+ "	 --------------------------------------------------------------------\n"
                		 								+ "MAT1	        MAT 1311 - Business Algebra           "+Capacity.MAT1+"\n"
                		 								+ "MAT2	        MAT 1312 - Business Calculus         "+Capacity.MAT2+"\n"
                		 								+ "MAT3	        MAT 2314 - Statistics                          "+Capacity.MAT3+"\n"
                		 								+ "MAT4	        MAT 2318 - Calculus                            "+Capacity.MAT4+"\n"
                		 								+ "\n"
                		 								+ "Enter 0 to return to previous screen."	 			
                		);
                                                 
                
String answer;

   switch(math.toUpperCase()){
     	case "MAT1":
     		if(AddCourseDB.classSchedule.contains("MAT1	        MAT 1311 - Business Algebra\n")){
        		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
     		}else if(Capacity.MAT1cap>29){
        		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
        	}else {
        		Capacity.MAT1cap++;
        		Capacity.MAT1 = Capacity.MAT1cap+"/30";
     	classSchedule.add("MAT1	        MAT 1311 - Business Algebra\n");
        	}
     	    break;
      case "MAT2":
    		if(AddCourseDB.classSchedule.contains("MAT2	        MAT 1312 - Business Calculus\n")){
        		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
    		}else if(Capacity.MAT2cap>29){
        		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
        	}else {
        		Capacity.MAT2cap++;
        		Capacity.MAT2 = Capacity.MAT2cap+"/30";
      	classSchedule.add("MAT2	        MAT 1312 - Business Calculus\n");
        	}
        	break;
      case "MAT3":
    		if(AddCourseDB.classSchedule.contains("MAT3	        MAT 2314 - Statistics\n")){
        		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
    		}else if(Capacity.MAT3cap>29){
        		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
        	}else {
        		Capacity.MAT3cap++;
        		Capacity.MAT3 = Capacity.MAT3cap+"/30";
      	classSchedule.add("MAT3	        MAT 2314 - Statistics\n");
        	}
     		break;
      case "MAT4":
    		if(AddCourseDB.classSchedule.contains("MAT4	        MAT 2318 - Calculus\n")){
        		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
    		}else if(Capacity.MAT4cap>29){
        		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
        	}else {
        		Capacity.MAT4cap++;
        		Capacity.MAT4 = Capacity.MAT4cap+"/30";
      	classSchedule.add("MAT4	        MAT 2318 - Calculus\n");
        	}
      	break;
      case "0":
      	addCourseMenu.options();
      	break;
      default:
        	AddCourseDB.math();
      	break;
     	}
 
   	 answer=  JOptionPane.showInputDialog("Add another course?\n"
      											   + "    Y/N   \n");
  	 
		
	if(answer.equalsIgnoreCase("y"))
		addCourseMenu.options();
		else 
		AddDropMenu.addDrop();
}

    public static void art() {

       String art= JOptionPane.showInputDialog("Select Art courses to add.\n"
											+ "Enter the CCN to add.\n"
											+ "	CCN   	    	 Course Description	        	           	    	 Capacity\n"
											+ "	 --------------------------------------------------------------------\n"
       		 								+ "ART1	        ART 1311 - Painting I                      "+Capacity.ART1+"\n"
       		 								+ "ART2	        ART 1312 - Painting II                     "+Capacity.ART2+"\n"
       		 								+ "ART3	        ART 2314 - Photography               "+Capacity.ART3+"\n"
       		 								+ "ART4	        ART 2318 - Drawing                        "+Capacity.ART4+"\n"
       		 								+ "\n"
       		 								+ "Enter 0 to return to previous screen."				                		 								
    		   );
                                        
       
       String answer;

       	switch(art.toUpperCase()){
       		case "ART1":
       			if(AddCourseDB.classSchedule.contains("ART1	        ART 1311 - Painting I\n")){
            		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
       			}else if(Capacity.ART1cap>29){
            		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
            	}else {
            		Capacity.ART1cap++;
            		Capacity.ART1 = Capacity.ART1cap+"/30";
       			classSchedule.add("ART1	        ART 1311 - Painting I\n");
            	}
       			break;
       		case "ART2":
       			if(AddCourseDB.classSchedule.contains("ART2	        ART 1312 - Painting II\n")){
            		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
       			}else if(Capacity.ART2cap>29){
            		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
            	}else {
            		Capacity.ART2cap++;
            		Capacity.ART2 = Capacity.ART2cap+"/30";
       			classSchedule.add("ART2	        ART 1312 - Painting II\n");
            	}
       			break;
       		case "ART3":
       			if(AddCourseDB.classSchedule.contains("ART3	        ART 2314 - Photography\n")){
            		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
       			}else if(Capacity.ART3cap>29){
            		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
            	}else {
            		Capacity.ART3cap++;
            		Capacity.ART3 = Capacity.ART3cap+"/30";
       			classSchedule.add("ART3	        ART 2314 - Photography\n");
            	}
       			break;
       		case "ART4":
       			if(AddCourseDB.classSchedule.contains("ART4	        ART 2318 - Drawing\n")){
            		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
       			}else if(Capacity.ART4cap>29){
            		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
            	}else {
            		Capacity.ART4cap++;
            		Capacity.ART4 = Capacity.ART4cap+"/30";
				classSchedule.add("ART4	        ART 2318 - Drawing\n");
            	}
				break;
       		case "0":
            	addCourseMenu.options();
            	break;
       		default:
       			AddCourseDB.art();
       			break;
       	}

       	answer=  JOptionPane.showInputDialog("Add another course?\n"
											   + "    Y/N   \n");


       	if(answer.equalsIgnoreCase("y"))
       		addCourseMenu.options();
       	else 
       		AddDropMenu.addDrop();
}

    public static void computerscience() {

       String cs= JOptionPane.showInputDialog("Select Computer Science courses to add.\n"
											+ "Enter the CCN to add.\n"
											+ "	CCN   	    	 Course Description	        	           	    	 Capacity\n"
											+ "	 --------------------------------------------------------------------\n"
       		 								+ "CSI1	        CSI 1311 - JAVA I                                "+Capacity.CSI1+"\n"
       		 								+ "CSI2	        CSI 1312 - JAVA II                               "+Capacity.CSI2+"\n"
       		 								+ "CSI3	        CSI 2314 - Systems Design              "+Capacity.CSI3+"\n"
       		 								+ "CSI4	        CSI 2318 - C++                                     "+Capacity.CSI4+"\n"
       		 								+ "\n"
       		 								+ "Enter 0 to return to previous screen."				                		 						
    		   );
                                        
        
       String answer;

				switch(cs.toUpperCase()){
				case "CSI1":
					if(AddCourseDB.classSchedule.contains("CSI1	        CSI 1311 - JAVA I\n")){
	            		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
					}else if(Capacity.CSI1cap>29){
	            		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
	            	}else {
	            		Capacity.CSI1cap++;
	            		Capacity.CSI1 = Capacity.CSI1cap+"/30";
					classSchedule.add("CSI1	        CSI 1311 - JAVA I\n");
	            	}
					break;
				case "CSI2":
					if(AddCourseDB.classSchedule.contains("CSI2	        CSI 1312 - JAVA II\n")){
	            		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
					}else if(Capacity.CSI2cap>29){
	            		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
	            	}else {
	            		Capacity.CSI2cap++;
	            		Capacity.CSI2 = Capacity.CSI2cap+"/30";
					classSchedule.add("CSI2	        CSI 1312 - JAVA II\n");
	            	}
					break;
				case "CSI3":
					if(AddCourseDB.classSchedule.contains("CSI3	        CSI 1314 - Systems Design\n")){
	            		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
					}else if(Capacity.CSI3cap>29){
	            		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
	            	}else {
	            		Capacity.CSI3cap++;
	            		Capacity.CSI3 = Capacity.CSI3cap+"/30";
					classSchedule.add("CSI3	        CSI 1314 - Systems Design\n");
	            	}
					break;
				case "CSI4":
					if(AddCourseDB.classSchedule.contains("CSI4	        CSI 2318 - C++\n")){
	            		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
					}else if(Capacity.CSI4cap>29){
	            		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
	            	}else {
	            		Capacity.CSI4cap++;
	            		Capacity.CSI4 = Capacity.CSI4cap+"/30";
					classSchedule.add("CSI4	        CSI 2318 - C++\n");
	            	}
					break;
				case "0":
	            	addCourseMenu.options();
	            	break;
				default:
					AddCourseDB.computerscience();
					break;
	}

				answer=  JOptionPane.showInputDialog("Add another course?\n"
											   + "    Y/N   \n");


				if(answer.equalsIgnoreCase("y"))
					addCourseMenu.options();
				else 
					AddDropMenu.addDrop();
}
    
    public static void chemistry() {

        String chemistry =JOptionPane.showInputDialog("Select Chemistry courses to add.\n"
											+ "Enter the CCN to add.\n"
											+ "	CCN   	    	 Course Description	        	           	    	           Capacity\n"
											+ "	 ------------------------------------------------------------------------------\n"
       		 								+ "CHM1	        CHM 1311 - General Chemistry I               "+Capacity.CHM1+"\n"
       		 								+ "CHM2	        CHM 1312 - General Chemistry II              "+Capacity.CHM2+"\n"
       		 								+ "CHM3	        CHM 2314 - Intermediate Chemistry        "+Capacity.CHM3+"\n"
       		 								+ "CHM4	        CHM 2318 - Volatile Chemistry                  "+Capacity.CHM4+"\n"
       		 								+ "\n"
       		 								+ "Enter 0 to return to previous screen."				                		 								
        		);
        
String answer;

	switch(chemistry.toUpperCase()){
		case "CHM1":
			if(AddCourseDB.classSchedule.contains("CHM1	        CHM 1311 - General Chemistry I\n")){
        		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
			}else if(Capacity.CHM1cap>29){
        		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
        	}else {
        		Capacity.CHM1cap++;
        		Capacity.CHM1 = Capacity.CHM1cap+"/30";
			classSchedule.add("CHM1	        CHM 1311 - General Chemistry I\n");
        	}
			break;
		case "CHM2":
			if(AddCourseDB.classSchedule.contains("CHM2	        CHM 1312 - General Chemistry II\n")){
        		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
			}else if(Capacity.CHM2cap>29){
        		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
        	}else {
        		Capacity.CHM2cap++;
        		Capacity.CHM2 = Capacity.CHM2cap+"/30";
			classSchedule.add("CHM2	        CHM 1312 - General Chemistry II\n");
        	}
			break;
		case "CHM3":
			if(AddCourseDB.classSchedule.contains("CHM3	        CHM 2314 - Intermediate Chemistry\n")){
        		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
			}else if(Capacity.CHM3cap>29){
        		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
        	}else {
        		Capacity.CHM3cap++;
        		Capacity.CHM3 = Capacity.CHM3cap+"/30";
			classSchedule.add("CHM3	        CHM 2314 - Intermediate Chemistry\n");
        	}
			break;
		case "CHM4":
			if(AddCourseDB.classSchedule.contains("CHM4	        CHM 2318 - Volatile Chemistry\n")){
        		JOptionPane.showMessageDialog(null, "This class is already registered. Please pick another class.", "Duplicate", 0);
			}else if(Capacity.CHM4cap>29){
        		JOptionPane.showMessageDialog(null, "This class has reached max capacity.");
        	}else {
        		Capacity.CHM4cap++;
        		Capacity.CHM4 = Capacity.CHM4cap+"/30";
			classSchedule.add("CHM4	        CHM 2318 - Volatile Chemistry\n");
        	}
			break;
		case "0":
        	addCourseMenu.options();
        	break;
		default:
			AddCourseDB.chemistry();
			break;
	}

	answer=  JOptionPane.showInputDialog("Add another course?\n"
											   + "    Y/N   \n");


	if(answer.equalsIgnoreCase("y"))
		addCourseMenu.options();
	else 
		AddDropMenu.addDrop();                                   
     
}
}
