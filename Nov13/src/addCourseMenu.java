import javax.swing.JOptionPane;

public class addCourseMenu {
		
		String message;
	
		//allows user to choose which classes they want to add
			
		public static void options()
		{
			 String option =  JOptionPane.showInputDialog("Which subject would you like to add? \n"


                                                    + "  1  - Accounting\n"

                                                    + "  2  - Government\n"
                                                    
                                                    + "  3  - History\n"

                                                    + "  4  - Management\n"

                                                    + "  5  - Math\n"
                                                    
				  									+ "  6  - Biology\n"
				  									
				  									+ "  7  - Chemistry\n"
				  									
				  									+ "  8  - Art\n"
				  									
				  									+ "  9  - Computer Science\n"
				  									
				  									+ "  0  - Return to previous screen"
				  );
     
			

		    	switch(option){
		    	case "1":
		    		AddCourseDB.accounting();
		    		break;
		       	case "2":
		       		AddCourseDB.government();
		       		break;
		       	case "3":
		       		AddCourseDB.history();
		       		break;
		       	case "4":
		       		AddCourseDB.management();
		       		break;
		       	case "5":
		       		AddCourseDB.math();
		       		break;
		       	case "6":
		       		AddCourseDB.biology();
		       		break;
		       	case "7":
		       		AddCourseDB.chemistry();
		       		break;
		       	case "8":
		       		AddCourseDB.art();
		       		break;
		       	case "9":
		       		AddCourseDB.computerscience();
		       		break;
		       	case "0":
		       		AddDropMenu.addDrop();
		       		break;
		       	default:
		       		addCourseMenu.options();
		         		break;
		    	}
		    

		}
		}
				
		
	

