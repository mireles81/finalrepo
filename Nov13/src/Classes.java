
import javax.swing.JOptionPane;

public class Classes {
	 
	
	//allows user to see which classes are available 
	
	public static void classes() {
		
		  String selection =  JOptionPane.showInputDialog("Enter a selection to view courses? \n"


                                                    + "  1 - Accounting\n"

                                                    + "  2 - Government\n"
                                                    
                                                    + "  3 - History\n"

                                                    + "  4 - Management\n"

                                                    + "  5 - Math\n"
                                                    
				  									+ "  6 - Biology\n"
				  									
				  									+ "  7 - Chemistry\n"
				  									
				  									+ "  8 - Art\n"
				  									
				  									+ "  9 - Computer Science\n"
				  									
				  									+ "  0 - Return to previous screen"
				  );
     
    	switch(selection){
    	case "1":
    		AvailableCourses.accounting();
    		break;
       	case "2":
       		AvailableCourses.government();
       		break;
       	case "3":
       		AvailableCourses.history();
       		break;
       	case "4":
       		AvailableCourses.management();
       		break;
       	case "5":
       		AvailableCourses.math();
       		break;
       	case "6":
       		AvailableCourses.biology();
       		break;
       	case "7":
       		AvailableCourses.chemistry();
       		break;
       	case "8":
       		AvailableCourses.art();
       		break;
       	case "9":
       		AvailableCourses.computerscience();
       		break;
       	case "0":
       		MainMenu.selection();
       		break;
       	default:
       		Classes.classes();
         		break;
    	}
    

}
}
		