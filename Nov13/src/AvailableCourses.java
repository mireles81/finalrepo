

import javax.swing.JOptionPane;

public class AvailableCourses {
		
	//Biology courses 
    public static void biology() {

                 JOptionPane.showMessageDialog(null, "Biology courses available.\n"
                		 								+ "	BIO 1311 - General Biology I\n"
                		 								+ " BIO 1312 - General Biology II\n"
                		 								+ " BIO 2412 - Organic Biology\n"
                		 								+ " BIO 2415 - Molecular Biology\n"
                		 								
                		 								+ " Hit Enter to return to previous screen");
                                                 
                 	Classes.classes();
}
    //Accounting classes
    public static void accounting() {

        JOptionPane.showMessageDialog(null, "Accounting courses available.\n"
       		 								+ "	ACC 1311 - Introduction to Accounting I\n"
       		 								+ " ACC 1312 - Introduction to Accounting II\n"
       		 								+ " ACC 2411 - Intermediate Accounting I\n"
       		 								+ " ACC 2412 - Intermediate Accounting II\n"
       		 								+ " ACC 2418 - Ethics in Accounting\n"
       		 								
       		 								+ " Hit Enter to return to previous screen");
                                        
        	Classes.classes();
}
 

    //History classes
    public static void history() {

                 JOptionPane.showMessageDialog(null, "History courses available.\n"
                		 								+ "	HIS 1311 - United States History I\n"
                		 								+ " HIS 1312 - United States History II\n"
                		 								+ " HIS 1314 - Texas History\n"
                		 								+ " HIS 2318 - History of Europe\n"
                		 					                		 								
                		 								+ " Hit Enter to return to previous screen");
                                                 
                 	Classes.classes();
}


	
    //Government classes
    public static void government() {

                 JOptionPane.showMessageDialog(null, "Government courses available.\n"
                		 								+ "	GOV 1311 - American Government I\n"
                		 								+ " GOV 1312 - American Government II\n"
                		 								+ " GOV 1314 - Texas Government\n"
                		 								+ " GOV 2318 - U.S. Electoral System\n"
                		 					                		 								
                		 								+ " Hit Enter to return to previous screen");
                                                 
                 	Classes.classes();
}
    

    //Management classes
    public static void management() {

                 JOptionPane.showMessageDialog(null, "Management courses available.\n"
                		 								+ "	MGT 1311 - Introduction to Management\n"
                		 								+ " MGT 1312 - Issues in Management\n"
                		 								+ " MGT 2314 - Science of Management\n"
                		 								+ " MGT 2318 - Ethics in Management\n"
                		 					                		 								
                		 								+ " Hit Enter to return to previous screen");
                                                 
                 	Classes.classes();
}
    
    //Math classes
    public static void math() {

                 JOptionPane.showMessageDialog(null, "Math courses available.\n"
                		 								+ "	MAT 1311 - Business Algebra\n"
                		 								+ " MAT 1312 - Business Calculus\n"
                		 								+ " MAT 2314 - Statistics\n"
                		 								+ " MAT 2318 - Calculus\n"
                		 					                		 								
                		 								+ " Hit Enter to return to previous screen");
                                                 
                 	Classes.classes();
}
    
    
    //Art classes
    public static void art() {

        JOptionPane.showMessageDialog(null, "Art courses available.\n"
       		 								+ "	ART 1311 - Painting I\n"
       		 								+ " ART 1312 - Painting II\n"
       		 								+ " ART 2314 - Photography\n"
       		 								+ " ART 2318 - Drawing\n"
       		 					                		 								
       		 								+ " Hit Enter to return to previous screen");
                                        
        	Classes.classes();
}
    
    
    //Computer Science classes
    public static void computerscience() {

        JOptionPane.showMessageDialog(null, "Computer Science courses available.\n"
       		 								+ "	CSI 1311 - JAVA I\n"
       		 								+ " CSI 1312 - JAVA II\n"
       		 								+ " CSI 2314 - Systems Design\n"
       		 								+ " CSI 2318 - C++\n"
       		 					                		 								
       		 								+ " Hit Enter to return to previous screen");
                                        
        	Classes.classes();
}
    
    //Chemistry classes
    public static void chemistry() {

        JOptionPane.showMessageDialog(null, "Chemistry courses available.\n"
       		 								+ "	CHM 1311 - General Chemistry I\n"
       		 								+ " CHM 1312 - General Chemistry II\n"
       		 								+ " CHM 2314 - Intermediate Chemistry\n"
       		 								+ " CHM 2318 - Volatile Chemistry\n"
       		 					                		 								
       		 								+ " Hit Enter to return to previous screen");
                                        
        	Classes.classes();
}
}
	
